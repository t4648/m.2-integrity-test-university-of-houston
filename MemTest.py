#! /usr/bin/python3

import subprocess
import time
import sys
import os
import serial 

class Assess:
	def writeSpd(self):
		ser = serial.Serial(
			"/dev/ttyUSB0",
			baudrate = 19200,
			timeout=3.0 , 
			rtscts=False, 
			dsrdtr=False
				)
		time.sleep(3)
		ser.flush()
		m2log=open("m2log.txt", 'a')
		m2=c=subprocess.Popen(['lsblk | grep sd'],stdout=m2log, stderr=m2log, shell=True)
		m2_file = open("m2log.txt", "r")
		lines1 = m2_file.readlines()
		last_lines1 = lines1[-1:]
		s=str(last_lines1)
		print(s)
		if "'sda" in s:
			log = open("testRes.txt", 'a')
			c=subprocess.Popen(['sudo sync; sudo dd if=/dev/zero of=/dev/sda bs=1M count=1024; sync', '/p'], stdout=log, stderr=log, shell=True)
			c.poll()
			while c.poll() is None:
				x = ser.readline().decode("utf-8", errors='replace')
				print(x)
				with open("p_log.txt", 'a') as out:
					out.write(x)
			c.wait()
		elif "'sdb" in s:
			log = open("testRes.txt", 'a')
			c=subprocess.Popen(['sudo sync; sudo dd if=/dev/zero of=/dev/sdb bs=1M count=1024; sync', '/p'], stdout=log, stderr=log, shell=True)
			c.poll()
			while c.poll() is None:
				x = ser.readline().decode("utf-8", errors='replace')
				print(x)
				with open("p_log.txt", 'a') as out:
					out.write(x)
			c.wait()	
		else:
			print("/dev/sda and /dev/sdb not found")	
			 
	def readSpd(self):
		ser = serial.Serial(
			"/dev/ttyUSB0",
			baudrate = 19200,
			timeout=3.0 , 
			rtscts=False, 
			dsrdtr=False
				)
		time.sleep(3)
		ser.flush()		
		m2log=open("m2log.txt", 'a')
		m2=c=subprocess.Popen(['lsblk | grep sd'],stdout=m2log, stderr=m2log, shell=True)
		m2_file = open("m2log.txt", "r")
		lines1 = m2_file.readlines()
		last_lines1 = lines1[-1:]
		s=str(last_lines1)
		if "'sda" in s:
			log1 = open("testRes.txt", 'a')		
			r=subprocess.Popen(['sudo dd if=/dev/sda of=/dev/null bs=1M count=1024'], stdout=log1, stderr=log1, shell=True)
			r.poll()
			while r.poll() is None:
				x = ser.readline().decode("UTF-8", errors='replace')
				print(x)
				with open("p_log.txt", 'a') as out:
					out.write(x)
			r.wait()
		elif "'sdb" in s:
			log1 = open("testRes.txt", 'a')		
			r=subprocess.Popen(['sudo dd if=/dev/sdb of=/dev/null bs=1M count=1024'], stdout=log1, stderr=log1, shell=True)
			r.poll()
			while r.poll() is None:
				x = ser.readline().decode("UTF-8", errors='replace')
				print(x)
				with open("p_log.txt", 'a') as out:
					out.write(x)
			r.wait()
		else:
			print("/dev/sda and /dev/sdb not found")			
		
	def clrCache(self):
		proc = subprocess.Popen(['sudo /sbin/sysctl -w vm.drop_caches=3'], shell=True)
		proc.wait()
		
	def pTest(self):
		ser = serial.Serial(
			"/dev/ttyUSB0",
			baudrate = 19200,
			timeout=2.0 , 
			rtscts=False, 
			dsrdtr=False
				)
		time.sleep(2)
		ser.flush()
		m2log=open("m2log.txt", 'a')
		m2=c=subprocess.Popen(['lsblk | grep sd'],stdout=m2log, stderr=m2log, shell=True)
		m2_file = open("m2log.txt", "r")
		lines1 = m2_file.readlines()
		last_lines1 = lines1[-1:]
		s=str(last_lines1)
		print(s)
		if "'sda" in s:
			log = open("testRes.txt", 'a')
			c=subprocess.Popen(['sudo sync; sudo dd if=/dev/zero of=/dev/sda bs=1M count=1024; sync', '/p'], stdout=log, stderr=log, shell=True)
			c.poll()
			while c.poll() is None:
				x = ser.readline().decode("utf-8", errors='replace')
				print(x)
				with open("p_log.txt", 'a') as out:
					out.write(x)
			c.wait()
		elif "'sdb" in s:
			log = open("testRes.txt", 'a')
			c=subprocess.Popen(['sudo sync; sudo dd if=/dev/zero of=/dev/sdb bs=1M count=1024; sync', '/p'], stdout=log, stderr=log, shell=True)
			c.poll()
			while c.poll() is None:
				x = ser.readline().decode("utf-8", errors='replace')
				print(x)
				with open("p_log.txt", 'a') as out:
					out.write(x)
			c.wait()	
		else:
			print("/dev/sda and /dev/sdb not found")
		
	def wSpeed(self):
		m2log=open("m2log.txt", 'a')
		m2=c=subprocess.Popen(['lsblk | grep sd'],stdout=m2log, stderr=m2log, shell=True)
		m2_file = open("m2log.txt", "r")
		lines1 = m2_file.readlines()
		last_lines1 = lines1[-1:]
		s=str(last_lines1)
		print(s)
		if "'sda" in s:
			log = open("testRes.txt", 'a')
			c=subprocess.Popen(['sudo sync; sudo dd if=/dev/zero of=/dev/sda bs=1M count=1024; sync', '/p'], stdout=log, stderr=log, shell=True)
			c.wait()
		elif "'sdb" in s:
			log = open("testRes.txt", 'a')
			c=subprocess.Popen(['sudo sync; sudo dd if=/dev/zero of=/dev/sdb bs=1M count=1024; sync', '/p'], stdout=log, stderr=log, shell=True)
			c.wait()
		else:
			print("/dev/sda and /dev/sdb not found")
			
	def rSpeed(self):
		m2log=open("m2log.txt", 'a')
		m2=c=subprocess.Popen(['lsblk | grep sd'],stdout=m2log, stderr=m2log, shell=True)
		m2_file = open("m2log.txt", "r")
		lines1 = m2_file.readlines()
		last_lines1 = lines1[-1:]
		s=str(last_lines1)
		if "'sda" in s:
			log1 = open("testRes.txt", 'a')		
			r=subprocess.Popen(['sudo dd if=/dev/sda of=/dev/null bs=1M count=1024'], stdout=log1, stderr=log1, shell=True)
			r.wait()
		elif "'sdb" in s:
			log1 = open("testRes.txt", 'a')		
			r=subprocess.Popen(['sudo dd if=/dev/sdb of=/dev/null bs=1M count=1024'], stdout=log1, stderr=log1, shell=True)
			r.wait()
		else:
			print("/dev/sda and /dev/sdb not found")	
					
class DataManager:
	def limFile(self, filename):
		self.filename=filename
		b=os.path.getsize(self.filename)
		print(b)		
		if b>= 2048:
			os.remove(self.filename)
			

#TEST SEQUENCE BELOW
#Test.writespd()
#Test.readSpd()

#clears cache to get true SSD speed
#Test.clrCache()

#Test.readSpd()

