#! /usr/bin/python3
import tkinter as tk
from tkinter import ttk
from tkinter import font as tkfont
from tkinter.messagebox import showinfo
import subprocess
import serial
import threading
import time
import os
import numpy as np
from PIL import ImageTk, Image

class Window(tk.Toplevel):
    def __init__(self, parent):
        super().__init__(parent) 
        #self.geometry('480x320')
        self.attributes("-fullscreen", True)
        self.grid_columnconfigure(6, weight=1)
        self.grid_rowconfigure(4, weight=1)
        self.configure(bg='white')
                
        self.image2path = "/home/pi/M2logo.png"
        self.image2 = Image.open(self.image2path)
        self.image2 = self.image2.resize((65, 65), Image.ANTIALIAS)
        self.test = ImageTk.PhotoImage(self.image2)
        self.label2 = tk.Label(self, image=self.test, bg='white')
        self.label2.place(x=400, y=0)
             
        helvfont = tkfont.Font(family='Helvetica', size=20, weight='bold')
        resfont = tkfont.Font(family='Helvetica', size=15, weight='bold')
        pfont = tkfont.Font(family='Helvetica', size=12, weight='normal')

        powerTest=True
        
        #back button
        self.button = tk.Button(self, 
        text = '<<Back',
        font=resfont,
        bg='orange',
        command = self.destroy).grid(column=0, columnspan=2, rowspan=1, row=0 ,ipadx=15, ipady= 15)
        
        #read button
        self.button = tk.Button(self, 
        text = 'Read\nTest',
        font=resfont,
        bg='orange',
        padx=25,
        command = self.readTest).place(x=0, y=260)

        #read results text
        self.rText = tk.Label(self, text="Read Speed:\nNO DATA  ", bg='white', font=resfont)
        self.rText.grid(column = 0, row=4, columnspan=1, rowspan=1  )
        
        #write button
        self.button = tk.Button(self, 
        text = 'Write\nTest',
        font=resfont,
        bg='orange',
        padx=25,
        command = self.writeTest).place(x=185, y=260)
        
        #Write results text
        self.wText = tk.Label(self, text="Write Speed:\nNO DATA", bg='white', font=resfont)
        self.wText.grid(column = 0, row=3, columnspan=2, rowspan=1)
        
        #power button
        self.button1 = tk.Button(self, 
        text = 'Power\nTest',
        font=resfont,
        bg='orange',
        padx=25,
        command=self.pTest).place(x=370,y=260)
        
        #power test results
        self.busV = tk.Label(self, text="Bus:", bg='white', font=resfont)
        self.busV.place(x=220, y=68)

        self.loadV=tk.Label(self, text="Load:", bg='white', font=resfont)
        self.loadV.place(x=405, y=68)
        
        self.current=tk.Label(self, text="Current:", bg='white', font=resfont)
        self.current.place(x=210, y=190)
        
        self.power=tk.Label(self, text="Power:", bg='white', font=resfont)
        self.power.place(x=395, y=190)
        
        
    def writeTest(self):
        from memTest import Assess, DataManager
        import numpy as np
        Test=Assess()
        lim=DataManager()
            
        Test.wSpeed()
        res_file1 = open("testRes.txt", "r")
        lines1 = res_file1.readlines()
        last_lines1 = lines1[-1:]
        writeRes=open("writeResults.txt", 'a')
        writeRes.writelines(last_lines1)
        writeRes.close()
        writeRes=open("writeResults.txt", 'r')
        strings=writeRes.readline().split(' ')
        strings=list(strings)
        writeVar =  'Write Speed:\n{} MB/s'.format(strings[9]) #sets text on window to new variable
        os.remove("writeResults.txt")
        self.wText.config(text=writeVar)
        
    def readTest(self):
        from memTest import Assess, DataManager
        import numpy as np
        Test=Assess()
        lim=DataManager()
            
        Test.clrCache()
        Test.rSpeed()
        res_file3 = open("testRes.txt", "r")
        lines3 = res_file3.readlines()
        last_lines3 = lines3[-1:]
        readRes1=open("readResults.txt", 'a')
        readRes1.writelines(last_lines3)
        readRes1.close()
        readRes1=open("readResults.txt", 'r')
        strings2=readRes1.readline().split(' ')
        strings2=list(strings2)
        readVar1 =  'Read Speed:\n{} MB/s'.format(strings2[9]) #sets text on window to new variable
        self.rText.config(text=readVar1)
        os.remove("readResults.txt")
        os.remove("testRes.txt")
        
    def pTest(self):
        from memTest import Assess, DataManager
        import numpy as np
        Test=Assess()
        lim=DataManager()
        vpath= '/home/pi/values.txt'
        if os.path.exists(vpath):
            os.remove(vpath)
		
		#write test
        Test.writeSpd()
		
		#read test
        Test.clrCache()
        Test.readSpd()
		
		#power meter averaging function
        with open('p_log.txt', 'r') as vfile:
            lines = vfile.readlines()
        with open('p_log.txt', 'w') as vfile:
                for line in lines:
                    if line.strip("\n") != "\x00":
                        vfile2 = open('values.txt', 'a')	
                        vfile2.writelines(line.rstrip('\x00'))
                        vfile2.close()
        bv= np.loadtxt("values.txt",encoding='UTF-8')[:, 0]
        sv= np.loadtxt("values.txt",encoding='UTF-8')[:, 1]
        lv= np.loadtxt("values.txt",encoding='UTF-8')[:, 2]
        c= np.loadtxt("values.txt",encoding='UTF-8')[:, 3]
        p= np.loadtxt("values.txt",encoding='UTF-8')[:, 4]
        sumbv = np.sum(bv)
        sumsv=np.sum(sv)
        sumlv=np.sum(lv)
        sumc=np.sum(c)
        sump=np.sum(p)
        bv_ave = sumbv / len(bv)
        sv_ave = sumsv / len(sv)
        lv_ave = sumlv / len(lv)
        c_ave = sumc / len(c)
        p_ave = sump / len(p)
        powerVar =  'Power:\n{}mW'.format(round(p_ave, 2))
        self.power.config(text=powerVar) #sets text on window to new variable
        cVar =  'Current:\n{}mA'.format(round(c_ave, 2))
        self.current.config(text=cVar) #sets text on window to new variable
        lVar =  'Load:\n{}v'.format(round(lv_ave,2))
        self.loadV.config(text=lVar) #sets text on window to new variable
        busVar =  'Bus:\n{}v'.format(round(bv_ave,2))
        self.busV.config(text=busVar) #sets text on window to new variable
        os.remove("values.txt")
        os.remove("p_log.txt")		
        lim.limFile("testRes.txt")
 
class MainApplication(tk.Tk):
    def __init__(self):
        super().__init__() 
        
        #configure root window
        self.title('M.2 SSD Test')
       # self.geometry('480x320') #size of 3.5 in LCD being used
        self.attributes("-fullscreen", True)
        self.grid_columnconfigure(4, weight=1)
        self.grid_rowconfigure(4, weight=1)
        self.configure(bg='white')

        #text styles
        helvfont = tkfont.Font(family='Helvetica', size=20, weight='bold')
        resFont = tkfont.Font(family='Helvetica', size=15, weight='bold')
		
        #Icon label
        self.image1path = "/home/pi/M2logo.png"
        self.image1 = Image.open(self.image1path)
        self.image1 = self.image1.resize((65, 65), Image.ANTIALIAS)
        self.test = ImageTk.PhotoImage(self.image1)
        self.label1 = tk.Label(self, image=self.test, bg='white')
        self.label1.place(x=400, y=0)
		
        #configure ssd text
        m2logssd=open("m2logssd.txt", 'a')
        m2=subprocess.Popen(['lsblk | grep sd'],stdout=m2logssd, stderr=m2logssd, shell=True)
        m2_file1 = open("m2logssd.txt", "r")
        lines1 = m2_file1.readlines()
        last_lines1 = lines1[-1:]
        m2str=open("m2logssd.txt", 'a')
        m2str.writelines(last_lines1)
        m2str.close()
        m2str=open("m2logssd.txt", 'r')
        stringsm2=m2str.readline().split(' ')
        stringsm2=list(stringsm2)
        writeVar2 =  format(stringsm2[2]) #sets text on window to new variable

        #Test button
        self.button = tk.Button(self,
        text = 'TEST SSD',
        font=helvfont,
        bg='orange',
        command = self.button_clicked1).grid(column=3,row=5 ,ipadx=2, ipady= 30)

	#Advanced testing button
        self.button = tk.Button(self, text = 'ADV>>',
        font=helvfont, 
        bg='orange', 
        command=self.button_clicked2).grid(column=5,row=5 ,ipadx=25, ipady= 30)
        
	#Write results text
        self.wText = tk.Label(self, text="Start Test", bg='white', font=resFont)
        self.wText.grid(column = 3, row=4, columnspan=4)

        #Read results text
        self.rText = tk.Label(self, text=" ", bg='white', font=resFont)
        self.rText.grid(column = 0, row=4, columnspan=4)

	#Power meter text
        self.pText = tk.Label(self, text=" ", bg='white', font=resFont)
        self.pText.grid(column = 5, row=4, columnspan=3)

	#SSD name display
        self.m2Text = tk.Label(self, text=" ", bg='white', font=resFont)
        self.m2Text.grid(column = 3, row=1, columnspan=3)
	#self.m2Text.config(text=writeVar2)
		
    def button_clicked1(self):
            m2logssd=open("m2logssd.txt", 'a')
            m2=subprocess.Popen(['lsblk | grep sd'],stdout=m2logssd, stderr=m2logssd, shell=True)
            m2_file1 = open("m2logssd.txt", "r")
            lines1 = m2_file1.readlines()
            last_lines1 = lines1[-1:]
            m2str=open("m2logssd.txt", 'a')
            m2str.writelines(last_lines1)
            m2str.close()
            m2str=open("m2logssd.txt", 'r')
            stringsm2=m2str.readline().split(' ')
            stringsm2=list(stringsm2)
            print(stringsm2)
            m2size=format(stringsm2[16])
            m2type=format(stringsm2[19])
            self.m2Text.config(text=m2size+" "+m2type) 
            
            from memTest import Assess, DataManager
            import numpy as np
            Test=Assess()
            lim=DataManager()
		
		#write test
            Test.writeSpd()
            res_file1 = open("testRes.txt", "r")
            lines1 = res_file1.readlines()
            last_lines1 = lines1[-1:]
            writeRes=open("writeResults.txt", 'a')
            writeRes.writelines(last_lines1)
            writeRes.close()
            writeRes=open("writeResults.txt", 'r')
            strings=writeRes.readline().split(' ')
            strings=list(strings)
            writeVar =  'Write Speed:\n{} MB/s'.format(strings[9]) #sets text on window to new variable
            os.remove("writeResults.txt")
            self.wText.config(text=writeVar)
		
		#read test
            Test.clrCache()
            Test.readSpd()
            res_file2 = open("testRes.txt", "r")
            lines2 = res_file2.readlines()
            last_lines2 = lines2[-1:]
            readRes=open("readResults.txt", 'a')
            readRes.writelines(last_lines2)
            readRes.close()
            readRes=open("readResults.txt", 'r')
            strings=readRes.readline().split(' ')
            strings=list(strings)
            readVar =  'Read Speed:\n{} MB/s'.format(strings[9]) #sets text on window to new variable
            self.rText.config(text=readVar)
            os.remove("readResults.txt")
		
		#power meter averaging function
            vpath= '/home/pi/values.txt'
            if os.path.exists(vpath):
                os.remove(vpath)
            else:
                pass
            with open('p_log.txt', 'r') as vfile:
                lines = vfile.readlines()
            with open('p_log.txt', 'w') as vfile:
                    for line in lines:
                        if line.strip("\n") != "\x00":
                            vfile2 = open('values.txt', 'a')	
                            vfile2.writelines(line.rstrip('\x00'))
                            vfile2.close()
            import numpy as np
            bv= np.loadtxt("values.txt",encoding='UTF-8')[:, 0]
            sv= np.loadtxt("values.txt",encoding='UTF-8')[:, 1]
            lv= np.loadtxt("values.txt",encoding='UTF-8')[:, 2]
            c= np.loadtxt("values.txt",encoding='UTF-8')[:, 3]
            p= np.loadtxt("values.txt",encoding='UTF-8')[:, 4]
            sumbv = np.sum(bv)
            sumsv=np.sum(sv)
            sumlv=np.sum(lv)
            sumc=np.sum(c)
            sump=np.sum(p)
            bv_ave = sumbv / len(bv)
            sv_ave = sumsv / len(sv)
            lv_ave = sumlv / len(lv)
            c_ave = sumc / len(c)
            p_ave = sump / len(p)
            powerVar =  'Power used:\n{} mW'.format(round(p_ave,2))
            self.pText.config(text=powerVar) #sets text on window to new variable
            os.remove("values.txt")
            os.remove("p_log.txt")		
            lim.limFile("testRes.txt")
		
    def button_clicked2(self):
        advWin = Window(self)
        advWin.grab_set()


if __name__ == "__main__":
    app = MainApplication()
    app.mainloop()
