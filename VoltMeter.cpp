#include <Wire.h>

#include "Adafruit_INA219.h"

Adafruit_INA219 ina219;
int t =0;
void setup() {

  // Open serial communications and wait for port to open:
  Serial.begin(19200);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  if (! ina219.begin()) {
    Serial.println("Failed to find INA219 chip");
    while (1) { delay(10); }
  }

 // Serial.print("BV"); Serial.print("\t"); // Bus Voltage
 // Serial.print("SV"); Serial.print("\t"); // Shunt Voltage
 // Serial.print("LV"); Serial.print("\t"); // Load Voltage
 // Serial.print("C"); Serial.print("\t");  // Current
  //Serial.println("P");  // Power
}

void loop() {
  float shuntvoltage = 0;
  float busvoltage = 0;
  float current_mA = 0;
  float loadvoltage = 0;
  float power_mW = 0;

  
  shuntvoltage = ina219.getShuntVoltage_mV();
  busvoltage = ina219.getBusVoltage_V();
  current_mA = ina219.getCurrent_mA();
  power_mW = ina219.getPower_mW();
  loadvoltage = busvoltage + (shuntvoltage / 1000);

  
  Serial.flush();
  // Bus Voltage
  ;Serial.print(busvoltage); Serial.print("\t");
  // Shunt Voltage
  Serial.print(shuntvoltage);Serial.print("\t");
  // Load Voltage
  Serial.print(loadvoltage);Serial.print("\t");
  // Current
  Serial.print(current_mA); Serial.print("\t");
  // Power
  Serial.println(power_mW);

  delay(400);

}
